package com.almoro.angela.mymovieapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ANGELA on 6/11/2015.
 */
public class ParseJSONTask extends AsyncTask<String, Void, Boolean> {
    private ProgressDialog dialog;

    private MovieListActivity activity;

    private Context context;
    private static String url = "https://dl.dropboxusercontent.com/u/5624850/movielist/list_movies_page1.json";

    private static final String M_MOVIES = "movies";
    private static final String M_DATA = "data";
    private static final String M_ID = "id";
    private static final String M_TITLE = "title";
    private static final String M_YEAR = "year";
    private static final String M_RATING = "rating";
    private static final String M_OVERVIEW = "overview";
    private static final String M_SLUG = "slug";

    private static ArrayList<HashMap<String, String>> movieList = new ArrayList<HashMap<String, String>>();
    private JSONArray movies;

    public ParseJSONTask(MovieListActivity activity) {
        this.activity = activity;
        context = activity;
        dialog = new ProgressDialog(context);
    }

    protected void onPreExecute() {
        this.dialog.setMessage("Loading movies");
        this.dialog.show();
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        final ArrayList<Movie> movieList = Movie.getMovies();

        MovieListAdapter adapter = new MovieListAdapter(context,movieList);

        MovieListFragment.adapterItems = adapter;
        MovieListFragment.listItems.setAdapter(adapter);

        if (this.dialog.isShowing()) {
            this.dialog.dismiss();
        }

        Log.d("Response: ", "> " + "2");
    }

    protected Boolean doInBackground(final String... args) {
        // Creating service handler class instance
        ServiceHandler sh = new ServiceHandler();

        // Making a request to url and getting response
        String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

        Log.d("Response: ", "> " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                // Getting JSON Array node
                movies = jsonObj.getJSONObject(M_DATA).getJSONArray(M_MOVIES);

                for (int i = 0; i < movies.length(); i++) {
                    JSONObject c = movies.getJSONObject(i);

                    String id = i+"";
                    String title = c.getString(M_TITLE);
                    String year = c.getInt(M_YEAR)+"";
                    String rating = c.getString(M_RATING)+"";
                    String overview = c.getString(M_OVERVIEW);
                    String slug = c.getString(M_SLUG);

                    Log.d("title: ", "> " + title);

                    Movie.addMovie(new Movie(id, title, year, rating, slug, overview));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("Error: ", "> " + "Couldn't get any data from the url");
        }

        return null;
    }
}