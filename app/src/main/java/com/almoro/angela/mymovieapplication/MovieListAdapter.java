package com.almoro.angela.mymovieapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ANGELA on 6/16/2015.
 *
 *
 */
public class MovieListAdapter extends ArrayAdapter<Movie> {
    private ArrayList<Movie> movieList;

    public MovieListAdapter(Context context, ArrayList<Movie> movieList){
        super(context, 0, movieList);
        this.movieList = movieList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_item, null);
        }

        final Movie item = getItem(position);

        if(item != null){
            final TextView title = (TextView) convertView.findViewById(R.id.movieTitle);
            final TextView year = (TextView) convertView.findViewById(R.id.movieYear);
            ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
            final String url = "https://dl.dropboxusercontent.com/u/5624850/movielist/images/"+item.getSlug()+"-backdrop.jpg";

            Picasso.with(getContext())
                    .load(url)
                    .noFade()
                    .fit()
                    .centerCrop()
                    .into(icon);

            title.setText(item.getTitle());
            year.setText(String.valueOf(item.getYear()));
        }

        return convertView;
    }

}