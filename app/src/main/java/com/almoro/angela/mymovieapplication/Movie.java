package com.almoro.angela.mymovieapplication;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ANGELA on 6/15/2015.
 */
public class Movie implements Serializable {
    private static final long serialVersionUID = -1213949467658913456L;
    private String id;
    private String title;
    private String year;
    private String rating;
    private String slug;
    private String overview;

    private static ArrayList<Movie> movieList = new ArrayList<Movie>();

    public Movie(String id, String title, String year, String rating, String slug, String overview) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.rating = rating;
        this.slug = slug;
        this.overview = overview;
    }

    public String getId(){
        return this.id;
    }
    public String getTitle() {
        return this.title;
    }
    public String getYear() {
        return this.year;
    }
    public String getRating() {
        return this.rating;
    }
    public String getSlug() {
        return this.slug;
    }
    public String getOverview() {
        return this.overview;
    }

    public static ArrayList<Movie> getMovies() {
        return movieList;
    }

    public static void addMovie(Movie movie){
        movieList.add(movie);
    }
}