package com.almoro.angela.mymovieapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

/**
 * Created by ANGELA on 6/15/2015.
 */
public class MovieDetailActivity extends ActionBarActivity {
    MovieDetailFragment fragmentMovieDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Fetch the item to display from bundle
        Movie movie = (Movie) getIntent().getSerializableExtra("movie");
        if (savedInstanceState == null) {
            // Insert detail fragment based on the item passed
            fragmentMovieDetail = MovieDetailFragment.newInstance(movie);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.movieDetailContainer, fragmentMovieDetail);
            ft.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //

            NavUtils.navigateUpTo(this, new Intent(this, MovieListFragment.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}